mod types;

use std::{path::Path, time::Duration};

use awc::{http::StatusCode, Client, ClientResponse};
use multipart_client_stream::{Body, Part};
use types::{Alias, DeleteToken, Image, ImageResponse, Upload, UploadResponse};
use url::Url;

#[actix_rt::main]
async fn main() -> color_eyre::Result<()> {
    color_eyre::install()?;

    let client = pict_rs_client();

    let endpoint: Url = "http://localhost:8080".parse()?;

    let mut readdir = tokio::fs::read_dir("./images/valid").await?;
    let mut valid_paths = Vec::new();
    while let Some(entry) = readdir.next_entry().await? {
        valid_paths.push(entry.path());
    }
    let images = upload_backgrounded(&client, &endpoint, &valid_paths)
        .await?
        .into_iter()
        .map(check_response)
        .collect::<color_eyre::Result<Vec<Vec<Image>>>>()?
        .into_iter()
        .flatten()
        .collect::<Vec<_>>();
    // TODO: head images
    // TODO: fetch images
    // TODO: fetch details
    // TODO: process images
    for Image {
        file, delete_token, ..
    } in images
    {
        delete_image(&client, &endpoint, &file, &delete_token).await?;
    }
    let images = check_response(upload_inline(&client, &endpoint, &valid_paths).await?)?;
    // TODO: head images
    // TODO: fetch images
    // TODO: fetch details
    // TODO: process images
    for Image {
        file, delete_token, ..
    } in images
    {
        delete_image(&client, &endpoint, &file, &delete_token).await?;
    }

    // TODO: Healthz

    // TODO: Invalid Images

    // TODO: Internal APIs

    println!("Hello, world!");
    Ok(())
}

fn pict_rs_client() -> Client {
    Client::builder()
        .disable_redirects()
        .add_default_header(("user-agent", "pict-rs-integration-tests v0.1.0"))
        .timeout(Duration::from_secs(30))
        .finish()
}

#[tracing::instrument(skip(client, images))]
async fn post_images<P: AsRef<Path>>(
    client: &Client,
    endpoint: &Url,
    images: &[P],
) -> color_eyre::Result<ClientResponse> {
    let mut builder = Body::builder();

    for image in images {
        let filename = image
            .as_ref()
            .file_name()
            .ok_or(color_eyre::eyre::eyre!("No file name"))?
            .to_str()
            .ok_or(color_eyre::eyre::eyre!("Invalid filename string"))?;

        let file = tokio::fs::File::open(image).await?;

        builder = builder.append(Part::new("images[]", file).filename(filename));
    }

    let body = builder.build();

    let response = client
        .post(endpoint.as_str())
        .insert_header(("content-type", body.content_type()))
        .send_stream(body)
        .await
        .map_err(|e| color_eyre::eyre::eyre!("{e}"))?;

    Ok(response)
}

#[tracing::instrument(skip(client, images))]
async fn upload_inline<P: AsRef<Path>>(
    client: &Client,
    endpoint: &Url,
    images: &[P],
) -> color_eyre::Result<ImageResponse> {
    let mut endpoint = endpoint.clone();
    endpoint.set_path("/image");

    let mut response = post_images(client, &endpoint, images).await?;

    let image_response: ImageResponse = response.json().await?;

    Ok(image_response)
}

#[tracing::instrument(skip(client, images))]
async fn upload_backgrounded<P: AsRef<Path>>(
    client: &Client,
    endpoint: &Url,
    images: &[P],
) -> color_eyre::Result<Vec<ImageResponse>> {
    let mut endpoint = endpoint.clone();
    endpoint.set_path("/image/backgrounded");

    let mut response = post_images(client, &endpoint, images).await?;

    let upload_response: UploadResponse = response.json().await?;

    let uploads = match upload_response {
        UploadResponse::Ok { uploads, .. } => uploads,
        UploadResponse::Error { msg } => return Err(color_eyre::eyre::eyre!("{msg}")),
    };

    let mut tasks = Vec::new();

    for upload in uploads {
        let client = client.clone();
        let mut endpoint = endpoint.clone();

        endpoint.set_path("/image/backgrounded/claim");

        tasks.push(actix_rt::spawn(async move {
            claim_image(&client, &endpoint, &upload).await
        }));
    }

    let mut responses = Vec::new();
    for task in tasks {
        responses.push(task.await??);
    }

    Ok(responses)
}

#[tracing::instrument(skip(client))]
async fn claim_image(
    client: &Client,
    endpoint: &Url,
    upload: &Upload,
) -> color_eyre::Result<ImageResponse> {
    loop {
        let mut response = client
            .get(endpoint.as_str())
            .timeout(Duration::from_secs(15))
            .query(upload)?
            .send()
            .await
            .map_err(|e| color_eyre::eyre::eyre!("{e}"))?;

        if response.status() == StatusCode::NO_CONTENT {
            continue;
        }

        return Ok(response.json().await?);
    }
}

async fn delete_image(
    client: &Client,
    endpoint: &Url,
    alias: &Alias,
    delete_token: &DeleteToken,
) -> color_eyre::Result<()> {
    let mut endpoint = endpoint.clone();
    endpoint.set_path(&format!("/image/delete/{delete_token}/{alias}"));
    let response = client
        .delete(endpoint.as_str())
        .send()
        .await
        .map_err(|e| color_eyre::eyre::eyre!("{e}"))?;

    if response.status() != StatusCode::NO_CONTENT {
        return Err(color_eyre::eyre::eyre!("Failed to delete image"));
    }

    Ok(())
}

fn check_response(response: ImageResponse) -> color_eyre::Result<Vec<Image>> {
    match response {
        ImageResponse::Ok { files, .. } => Ok(files),
        ImageResponse::Error { msg } => Err(color_eyre::eyre::eyre!("{msg}")),
    }
}
