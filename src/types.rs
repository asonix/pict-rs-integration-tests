use serde::de::Error;
use time::OffsetDateTime;
use uuid::Uuid;

#[derive(Debug, serde::Deserialize)]
#[serde(untagged)]
pub(crate) enum UploadResponse {
    Ok {
        #[allow(unused)]
        msg: OkString,
        uploads: Vec<Upload>,
    },
    Error {
        msg: String,
    },
}

#[derive(Debug, serde::Deserialize)]
#[serde(untagged)]
pub(crate) enum ImageResponse {
    Ok {
        #[allow(unused)]
        msg: OkString,
        files: Vec<Image>,
    },
    Error {
        msg: String,
    },
}

#[derive(Debug, serde::Deserialize, serde::Serialize)]
pub(crate) struct Upload {
    pub(crate) upload_id: Uuid,
}

#[derive(Debug, serde::Deserialize)]
pub(crate) enum OkString {
    #[serde(rename = "ok")]
    Ok,
}

#[derive(Debug, serde::Deserialize, serde::Serialize)]
pub(crate) struct Image {
    pub(crate) delete_token: DeleteToken,
    pub(crate) file: Alias,
    pub(crate) details: Details,
}

#[derive(Debug, serde::Deserialize, serde::Serialize)]
#[serde(untagged)]
pub(crate) enum DeleteToken {
    Uuid(Uuid),
    Old(String),
}

#[derive(Debug, serde::Deserialize, serde::Serialize)]
#[serde(untagged)]
pub(crate) enum Alias {
    Alias(AliasInner),
    Old(String),
}

#[derive(Debug)]
pub(crate) struct AliasInner {
    uuid: Uuid,
    extension: String,
}

#[derive(Debug)]
pub(crate) struct AliasError;

#[derive(Debug, serde::Deserialize, serde::Serialize)]
pub(crate) struct Details {
    pub(crate) width: u16,
    pub(crate) height: u16,
    pub(crate) frames: Option<u32>,
    pub(crate) content_type: Mime,
    #[serde(with = "time::serde::rfc3339")]
    pub(crate) created_at: OffsetDateTime,
}

#[derive(Debug)]
pub(crate) struct Mime(mime::Mime);

impl std::ops::Deref for Mime {
    type Target = mime::Mime;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl AsRef<mime::Mime> for Mime {
    fn as_ref(&self) -> &mime::Mime {
        &self.0
    }
}

impl std::str::FromStr for AliasInner {
    type Err = AliasError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        if let Some((left, right)) = s.split_once('.') {
            let uuid = left.parse::<Uuid>().map_err(|_| AliasError)?;

            Ok(AliasInner {
                uuid,
                extension: String::from(right),
            })
        } else {
            Err(AliasError)
        }
    }
}

impl std::fmt::Display for Alias {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::Alias(inner) => inner.fmt(f),
            Self::Old(s) => s.fmt(f),
        }
    }
}

impl std::fmt::Display for DeleteToken {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::Uuid(u) => u.fmt(f),
            Self::Old(s) => s.fmt(f),
        }
    }
}

impl std::fmt::Display for AliasInner {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}.{}", self.uuid, self.extension)
    }
}

impl std::fmt::Display for AliasError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "Invalid alias format")
    }
}

impl std::error::Error for AliasError {}

impl<'de> serde::Deserialize<'de> for AliasInner {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: serde::Deserializer<'de>,
    {
        let s = String::deserialize(deserializer)?;

        s.parse::<AliasInner>().map_err(D::Error::custom)
    }
}

impl serde::Serialize for AliasInner {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        let s = self.to_string();

        String::serialize(&s, serializer)
    }
}

impl<'de> serde::Deserialize<'de> for Mime {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: serde::Deserializer<'de>,
    {
        let s = String::deserialize(deserializer)?;

        s.parse::<mime::Mime>().map_err(D::Error::custom).map(Mime)
    }
}

impl serde::Serialize for Mime {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        let s = self.to_string();

        String::serialize(&s, serializer)
    }
}

#[cfg(test)]
mod tests {
    #[test]
    fn deserialize_ok() {
        #[derive(serde::Deserialize)]
        struct TestStruct {
            #[serde(rename = "msg")]
            _msg: super::OkString,
        }

        let _: TestStruct = serde_json::from_str(r#"{"msg":"ok"}"#).expect("Deserialized");
    }

    #[test]
    fn deserialize_image_response() {
        let response: super::ImageResponse =
            serde_json::from_str(r#"{"msg": "ok", "files": []}"#).expect("Deserialized");

        assert!(matches!(response, super::ImageResponse::Ok { .. }))
    }
}
